package finastra.goal.pluralsight.creational.prototypepattern;

import java.util.HashMap;
import java.util.Map;

public class Registry
{
	private Map<String, Item> items = new HashMap<String, Item>();

	public Registry()
	{
		loadItems();
	}

	public Item createItem(String type)
	{
		Item item = null;
		
		try
		{
			item = (Item) (items.get(type)).clone();
		}
		catch (CloneNotSupportedException e)
		{
			e.printStackTrace();
		}

		return item;
	}
	
	private void loadItems()
	{
		Movie movie = new Movie();
		movie.setTitle("Asdfmovie");
		movie.setPrice(22.59);
		movie.setRuntime("2 hours");
		items.put("Movie", movie);
		
		Book book = new Book();
		book.setTitle("Asdfmovie");
		book.setPrice(22.59);
		book.setNumberOfPages(330);
		items.put("Book", book);

	}

}
