package finastra.goal.pluralsight.creational.builderpattern;

public class LunchOrderBuilderDemo
{

	public static void main(String[] args)
	{
		LunchOrderBuilder.Builder builder = new LunchOrderBuilder.Builder();
		builder.bread("Wheat").condiments("Lettuce").dressing("Mustard").meat("Ham");

		LunchOrderBuilder lunchOrder = builder.build();

		//		lunchOrderBean.setBread("Wheat");
		//		lunchOrderBean.setCondiments("lettuce");
		//		lunchOrderBean.setDressing("Mustard");
		//		lunchOrderBean.setMeat("Ham");

		System.out.println(lunchOrder.getBread());
		System.out.println(lunchOrder.getCondiments());
		System.out.println(lunchOrder.getDressing());
		System.out.println(lunchOrder.getMeat());

	}

}
