package finastra.goal.pluralsight.creational.builderpattern;

public class BuilderEverydayDemo
{

	public static void main(String[] args)
	{
		StringBuilder builder = new StringBuilder();

		builder.append("1 ");
		builder.append("+");
		builder.append(" 2");
		builder.append(" = ");
		builder.append("3");

		System.out.println(builder.toString());
	}

}
