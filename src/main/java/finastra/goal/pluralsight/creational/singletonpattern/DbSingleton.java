package finastra.goal.pluralsight.creational.singletonpattern;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import oracle.jdbc.driver.OracleDriver;

public class DbSingleton
{
	private static volatile DbSingleton instance = null;
	private static volatile Connection conn = null;

	private DbSingleton()
	{
		try
		{
			DriverManager.registerDriver(new OracleDriver());
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		if (conn != null)
		{
			throw new RuntimeException("Use getConnection() method to create!");
		}

		if (instance != null)
		{
			throw new RuntimeException("Use getInstance() method to create!");
		}
	}

	public static DbSingleton getInstance()
	{
		if (instance == null)
		{
			synchronized (DbSingleton.class)
			{
				if (instance == null)
				{
					instance = new DbSingleton();
				}
			}
		}

		return instance;
	}

	public Connection getConnection()
	{
		if (conn == null)
		{
			synchronized (DbSingleton.class)
			{
				if (conn == null)
				{
					try
					{
						String dbUrl = "jdbc:oracle:thin:@localhost:1521:orcl";
						String userNameAndPSW = "ckb";
						conn = DriverManager.getConnection(dbUrl, userNameAndPSW, userNameAndPSW);
					}
					catch (SQLException e)
					{
						e.printStackTrace();
					}
				}
			}
		}

		return conn;
	}
}
