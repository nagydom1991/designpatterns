package finastra.goal.pluralsight.creational.factorypattern;

public enum WebsiteType
{
	BLOG, SHOP;
}
